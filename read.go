package main

import (
    "fmt"
	"time"

    "github.com/gvalkov/golang-evdev"
)

const (
	NONE = iota
	UP
	RIGHT
	DOWN
	LEFT
	NORTH
	EAST
	SOUTH
	WEST
	META_LEFT
	META_RIGHT
	SELECT
	START
)

type abskey struct {
	xprev bool
	xval int32
	yprev bool
	yval int32
}

func (a *abskey) event(evt evdev.InputEvent) int {
	if evt.Code == evdev.ABS_X {
		return a.x(evt.Value)
	} else {
		return a.y(evt.Value)
	}
}

func (a *abskey) x(val int32) int {
	if a.xprev {
		a.xprev = false
		if a.xval - val > 0 {
			return RIGHT
		} else {
			return LEFT
		}
	} else {
		a.xprev = true
		a.xval = val
		return NONE
	}
	return NONE
}

func (a *abskey) y(val int32) int {
	if a.yprev {
		a.yprev = false
		if a.yval - val > 0 {
			return DOWN
		} else {
			return UP
		}
	} else {
		a.yprev = true
		a.yval = val
		return NONE
	}
	return NONE
}

var abs abskey

func other(evt evdev.InputEvent) int {
	if evt.Value == 0 { // key up
		switch evt.Code {
		case evdev.BTN_NORTH:
			return NORTH
		case evdev.BTN_EAST:
			return EAST
		case evdev.BTN_SOUTH:
			return SOUTH
		case evdev.BTN_C:
			return WEST
		case evdev.BTN_Z:
			return META_RIGHT
		case evdev.BTN_WEST:
			return META_LEFT
		case evdev.BTN_TL2:
			return SELECT
		case evdev.BTN_TR2:
			return START
		}
	}
	return NONE
}

func key(evts []evdev.InputEvent) int {
	for _, evt := range evts {
		if evt.Type == evdev.EV_ABS {
			return abs.event(evt)
		} else if evt.Type == evdev.EV_KEY {
			fmt.Println("t,c,v", evt.Type, evt.Code, evt.Value)
			return other(evt)
		} else {
		}
	}
	return NONE
}

func main() {
    device, err := evdev.Open("/dev/input/event0")
    if err != nil {
		fmt.Println("device err", err)
		return
	}

    fmt.Println(device)
    for {
        evts, err := device.Read()
        if err != nil {
            fmt.Println("event err", err)
			time.Sleep(time.Second * 1)
            continue
        }
		k := key(evts)
		switch k {
			case UP:
				fmt.Println("up")
			case RIGHT:
				fmt.Println("right")
			case DOWN:
				fmt.Println("down")
			case LEFT:
				fmt.Println("left")
			case NORTH:
				fmt.Println("north")
			case EAST:
				fmt.Println("east")
			case SOUTH:
				fmt.Println("south")
			case WEST:
				fmt.Println("west")
			case META_RIGHT:
				fmt.Println("meta right")
			case META_LEFT:
				fmt.Println("meta left")
			case SELECT:
				fmt.Println("select")
			case START:
				fmt.Println("start")
			default:
				// fmt.Println("uknown")
		}
    }
}

